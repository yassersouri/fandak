# TODO

## DataParallel

 - [x] Create subclass of nn.DataParallel to make DataParallel work for GeneralDataClass


## Tests

 - [ ] Metrics
    - [ ] Use Mock to mock the writer and test Metrics and MetricsCollection.
 - [ ] Trainer
    - [ ] Make sure than num_epochs, etc. are correct after resumes, etc.